﻿#pragma strict

private var power = 2000;

var bullet : Transform;
var target : Transform;
var spPoint : Transform;
var explosion : Transform;
var snd : AudioClip;

private var ftime : float = 0.0;

function Start () 
{

}

function Update () 
{
	transform.LookAt(target);
	ftime += Time.deltaTime;
	
	var hit : RaycastHit;
	var fwd = transform.TransformDirection(Vector3.forward);
	
	Debug.DrawRay(spPoint.transform.position, fwd * 20, Color.green);
	if(Physics.Raycast(spPoint.transform.position, fwd, hit, 20) == false) return;
	if(hit.collider.gameObject.tag !=  "TANK" || ftime < 2) return;
	Debug.Log(hit.collider.gameObject.name);
	
	Instantiate(explosion, spPoint.transform.position, spPoint.transform.rotation);
	
	var obj = Instantiate(bullet, spPoint.transform.position, Quaternion.identity);
	obj.rigidbody.AddForce(fwd * power);
	
	AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);
	ftime = 0;
}